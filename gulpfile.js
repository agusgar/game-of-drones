var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var cssnano = require('gulp-cssnano');

gulp.task('app-config', function() {
  return gulp.src(['public/app/config/app.routes.js', 'public/app/config/app.config.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.config.js'))
    .pipe(gulp.dest('public/dist'));
});

gulp.task('app-scripts', function() {
  return gulp.src('public/app/modules/*/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(concat('app.scripts.js'))
    .pipe(gulp.dest('public/dist'))
    .pipe(rename('app.scripts.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/dist'));
});

gulp.task('app-css', function() {
  return gulp.src('public/app/modules/*/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('app.css'))
    .pipe(gulp.dest('public/dist'))
    .pipe(rename('app.min.css'))
    .pipe(cssnano())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/dist'));
});

gulp.task('lint', function() {
  return gulp.src('public/app/modules/*/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('default', ['app-config', 'app-scripts', 'app-css', 'lint']);

//gulp.task('default', function() {
//
//});