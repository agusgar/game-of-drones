(function() {
  'use strict';

  angular.module('app.core', []);

  angular.module('GameOfDrones', [
    'ngRoute',
    'ngResource',
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'app.routes',
    'app.core',
    'ui.bootstrap'
  ]);
}());
