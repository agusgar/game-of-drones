
angular.module('app.core').directive('godAcceptAction', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionTitle', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionText', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionQuestion', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godModalTrigger', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionMake', ['$uibModal', function($uibModal) {
  return {
    restrict: 'A',
    require: [
      '^godDecisionTitle',
      '^godDecisionText',
      '^godDecisionQuestion',
      '^godAcceptAction',
      '^godModalTrigger'
    ],
    scope: {
      godDecisionTitle: '=',
      godDecisionText: '=',
      godDecisionQuestion: '=',
      godAcceptAction: '&',
      godModalTrigger: '='
    },
    controller: ['$scope', function($scope) {}],
    link: function(scope, iElement, iAttrs, ctrl) {
      scope.values = {
        decisionTitle: scope.godDecisionTitle,
        decisionText: scope.godDecisionText,
        decisionQuestion: scope.godDecisionQuestion
      };

      scope.$watch('godModalTrigger', function(newVal, oldVal) {
        if (newVal != oldVal) {
          var modalDecision = $uibModal.open({
            templateUrl: '/resource/ask/ask.tpl.html',
            controller: 'AskCtrl',
            size: 'md',
            scope: scope,
            resolve: {
              modalTitle: function() {
                return scope.values.decisionTitle;
              },
              modalText: function() {
                return scope.values.decisionText;
              },
              modalQuestion: function() {
                return scope.values.decisionQuestion;
              }
            }
          });

          modalDecision.result.then(function (accept) {
            if (accept) {
              scope.godAcceptAction();
            }
          });
        }
      });
    }
  };
}]);