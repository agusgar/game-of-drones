(function() {
  'use strict';

  angular.module('app.core').controller('AskCtrl', ['$scope', '$uibModalInstance', 'modalTitle', 'modalText', 'modalQuestion', function($scope, $uibModalInstance, modalTitle, modalText, modalQuestion) {
    $scope.modalTitle = modalTitle;
    $scope.modalText = modalText;
    $scope.modalQuestion = modalQuestion;

    $scope.accept = function () {
      $uibModalInstance.close(true);
    };
    $scope.cancel = function() {
      $uibModalInstance.close(false);
    };
  }]);
}());

