(function() {
  'use strict';

  angular.module('app.core').factory('GamesFct', ['$resource', function($resource) {
    return $resource('/rest/games/:id', null, {
      'update': { method:'PUT' }
    });
  }]);
}());

