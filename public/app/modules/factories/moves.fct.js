(function() {
  'use strict';

  angular.module('app.core').factory('MovesFct', ['$resource', function($resource) {
    return $resource('/rest/moves/:id', null, {
      'update': { method:'PUT' }
    });
  }]);
}());

