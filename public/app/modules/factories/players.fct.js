(function() {
  'use strict';

  angular.module('app.core').factory('PlayersFct', ['$resource', function($resource) {
    return $resource('/rest/players/:id', null, {
      'update': { method:'PUT' }
    });
  }]);
}());

