(function() {
  'use strict';

  angular.module('app.core').service('NotificationsSrv', function() {

    this.getNotifications = function() {
      return {
        success: {
          active: false,
          text: ''
        },
        error: {
          active: false,
          text: ''
        },
        info: {
          active: false,
          text: ''
        },
        warning: {
          active: false,
          text: ''
        },
        clearPromise: undefined
      };
    };

  });
}());
