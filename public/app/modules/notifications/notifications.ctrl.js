(function(){
  'use strict';

  angular.module('app.core').controller('NotificationsCtrl', ['$scope', '$rootScope', '$timeout', 'NotificationsSrv', function($scope, $rootScope, $timeout, NotificationsSrv) {
    $scope.notifications = NotificationsSrv.getNotifications();

    $rootScope.showSuccess = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'success');
    };

    $rootScope.showError = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'error');
    };

    $rootScope.showInfo = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'info');
    };

    $rootScope.showWarning = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'warning');
    };

    $rootScope.clearNotifications = function() {
      clearNotifications();
    };

    function setAndClearNotifications(text, time, type) {
      clearNotifications();
      if (time !== 0 && !time) {
        time = 15000;
      }
      $scope.notifications[type].text = text;
      $scope.notifications[type].active = true;
      $scope.notifications.clearPromise = $timeout(function() {
        $scope.notifications[type].text = "";
        $scope.notifications[type].active = false;
      }, time);
    }

    function clearNotifications() {
      $timeout.cancel($scope.notifications.clearPromise);

      $scope.notifications.success.text = "";
      $scope.notifications.error.text = "";
      $scope.notifications.info.text = "";
      $scope.notifications.warning.text = "";

      $scope.notifications.success.active = false;
      $scope.notifications.error.active = false;
      $scope.notifications.info.active = false;
      $scope.notifications.warning.active = false;
    }


  }]);
}());