(function() {
  'use strict';

  angular.module('app.core').service('ScoresSrv', [function() {
    this.getPlayerScores = function(games, players) {
      var scores = {};
      for(var i = 0; i < players.length; i++) {
        if (!scores[players[i]._id]) {
          scores[players[i]._id] = {
            victories: 0,
            played: 0
          };
        }
      }

      for(var j = 0; j < games.length; j++) {
        var game = games[j];
        var playerOne = game.playerOne;
        var playerTwo = game.playerTwo;
        var winner = game.winner;

        scores[playerOne].played += 1;
        scores[playerTwo].played += 1;

        if (playerOne == winner) {
          scores[playerOne].victories += 1;
        } else if (playerTwo == winner) {
          scores[playerTwo].victories += 1;
        }
      }

      return scores;
    };
  }]);
}());