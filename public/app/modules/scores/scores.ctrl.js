(function() {
  'use strict';

  angular.module('app.core').controller('ScoresCtrl', ['$scope', '$q', 'PlayersFct', 'GamesFct', 'ScoresSrv', function($scope, $q, PlayersFct, GamesFct, ScoresSrv) {
    $scope.players = PlayersFct.query();
    $scope.games = GamesFct.query();

    $q.all([$scope.players.$promise, $scope.games.$promise]).then(function() {
      $scope.scores = ScoresSrv.getPlayerScores($scope.games, $scope.players);
    });

  }]);
}());