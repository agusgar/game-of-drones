(function() {
  'use strict';

  angular.module('app.core').controller('OptionsCtrl', ['$scope', 'MovesFct', '$uibModal', 'OptionsSrv', function($scope, MovesFct, $uibModal, OptionsSrv) {
    function init() {
      $scope.moves = MovesFct.query();
    }

    $scope.openCreateMove = function(move) {
      var newMove = {name: '', kill: $scope.moves[0]._id};
      var modalMoveEdition = $uibModal.open({
        templateUrl: '/resource/options/moveEdition.tpl.html',
        controller: 'MoveEditionCtrl',
        size: 'lg',
        resolve: {
          move: function() {
            return newMove;
          },
          moves: function() {
            return $scope.moves;
          },
          title: function() {
            return 'New Move';
          }
        }
      });

      modalMoveEdition.result.then(function(newMove) {
        MovesFct.save(newMove).$promise.then(function(response) {
          $scope.moves = MovesFct.query();
        });
      });
    };

    $scope.openUpdateMove = function(move) {
      var auxMove = angular.copy(move);

      var modalMoveEdition = $uibModal.open({
        templateUrl: '/resource/options/moveEdition.tpl.html',
        controller: 'MoveEditionCtrl',
        size: 'lg',
        resolve: {
          move: function() {
            return auxMove;
          },
          moves: function() {
            return $scope.moves;
          },
          title: function() {
            return 'Edit Move';
          }
        }
      });

      modalMoveEdition.result.then(function(updatedMove) {
        MovesFct.update({id: move._id}, updatedMove).$promise.then(function(response) {
          angular.copy(updatedMove, move);
        });
      });
    };

    $scope.getMoveById = function(moveId) {
      return OptionsSrv.getMoveById(moveId, $scope.moves);
    };

    init();
  }]);
}());