(function() {
  'use strict';

  angular.module('app.core').controller('MoveEditionCtrl', ['$scope', '$uibModalInstance', 'move', 'moves', 'title', function($scope, $uibModalInstance, move, moves, title) {
    $scope.move = move;
    $scope.title = title;
    $scope.moves = moves;

    $scope.save = function() {
      $uibModalInstance.close($scope.move);
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);
}());