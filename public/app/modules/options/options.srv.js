(function() {
  'use strict';

  angular.module('app.core').service('OptionsSrv', [function() {
    this.getMoveById = function(moveId, moves) {
      var move;
      for(var i = 0; i < moves.length && !move; i++) {
        if (moves[i]._id == moveId) {
          move = moves[i];
        }
      }
      return move;
    };

  }]);
}());