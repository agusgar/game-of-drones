(function() {
  'use strict';

  angular.module('app.core').service('RoundSrv', [function() {
    this.getNewRound = function() {
      return {
        "movePlayerOne" : undefined,
        "movePlayerTwo" : undefined,
        "winner" : undefined
      };
    };

    this.getWinner = function(round, playerOne, playerTwo) {
      if (round.movePlayerOne.kill == round.movePlayerTwo._id) {
        return playerOne._id;
      } else if (round.movePlayerTwo.kill == round.movePlayerOne._id) {
        return playerTwo._id;
      } else {
        return undefined;
      }
    };
  }]);
}());