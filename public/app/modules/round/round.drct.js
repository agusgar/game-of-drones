//(function() {
//  'use strict';
//
//  angular.module('app.core').controller('RoundCtrl', ['$scope', 'RoundSrv', 'MovesFct', function($scope, RoundSrv, MovesFct) {
//    $scope.moves = MovesFct.query();
//
//    $scope.finishMove = function(move) {
//      if (!$scope.currentRound.movePlayerOne) {
//        $scope.currentRound.movePlayerOne = move;
//      } else {
//        $scope.currentRound.movePlayerTwo = move;
//        $scope.currentRound.winner = RoundSrv.getWinner();
//        $scope.finishRound($scope.currentRound);
//      }
//    };
//  }]);
//}());

angular.module('app.core').directive('godGame', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godCurrentRound', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godPlayerOne', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godPlayerTwo', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godFinishRound', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godRound', ['RoundSrv', 'MovesFct', function(RoundSrv, MovesFct) {
  return {
    restrict: 'A',
    templateUrl: '/resource/round/round.tpl.html',
    require: [
      '^godGame',
      '^godCurrentRound',
      '^godPlayerOne',
      '^godPlayerTwo',
      '^godFinishRound'
    ],
    scope: {
      godGame: '=',
      godCurrentRound: '=',
      godPlayerOne: '=',
      godPlayerTwo: '=',
      godFinishRound: '&'
    },
    controller: ['$scope', function($scope) {}],
    link: function(scope, iElement, iAttrs, ctrl) {
      scope.moves = MovesFct.query();
      scope.moves.$promise.then(function() {
        scope.move = scope.moves[0];
      });

      scope.finishMove = function(move) {
        if (!scope.godCurrentRound.movePlayerOne) {
          scope.godCurrentRound.movePlayerOne = move;
        } else {
          scope.godCurrentRound.movePlayerTwo = move;
          scope.godCurrentRound.winner = RoundSrv.getWinner(scope.godCurrentRound, scope.godPlayerOne, scope.godPlayerTwo);
          if (scope.godCurrentRound.winner) {
            console.log('Current round winner: ' + scope.godCurrentRound.winner);
            scope.godFinishRound();
          } else {
            scope.godCurrentRound.movePlayerOne = undefined;
            scope.godCurrentRound.movePlayerTwo = undefined;
          }
        }
        scope.move = scope.moves[0];
      };
    }
  };
}]);