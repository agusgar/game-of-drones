(function() {
  'use strict';

  angular.module('app.core').service('PlaySrv', [function() {
    this.getNewGame = function() {
      return {
        "playerOne" : undefined,
        "playerTwo" : undefined,
        "started" : undefined,
        "finalized" : undefined,
        "winner": undefined,
        "rounds" : []
      };
    };

    this.getWinner = function(game) {
      var players = {};
      players[game.playerOne] = 0;
      players[game.playerTwo] = 0;
      var winner;
      for (var i = 0; i < game.rounds.length && !winner; i++) {
        var round = game.rounds[i];
        if (round.winner) {
          players[round.winner] += 1;
          if (players[round.winner] >= 3) {
            winner = round.winner;
          }
        }
      }
      return winner;
    };

    this.getPlayerById = function(playerId, players) {
      var player;
      for(var i = 0; i < players.length && !player; i++) {
        if (players[i]._id == playerId) {
          player = players[i];
        }
      }
      return player;
    };

  }]);
}());