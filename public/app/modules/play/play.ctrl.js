(function() {
  'use strict';

  angular.module('app.core').controller('PlayCtrl', ['$scope', '$q', 'PlaySrv', 'RoundSrv', 'GamesFct', 'PlayersFct', function($scope, $q, PlaySrv, RoundSrv, GamesFct, PlayersFct) {
    function init() {
      $scope.newGame();
    }

    $scope.newGame = function() {
      $scope.game = PlaySrv.getNewGame();
      $scope.players = PlayersFct.query();
      $scope.playerOneName= "";
      $scope.playerTwoName = "";
      $scope.playerOne = undefined;
      $scope.playerTwo = undefined;
      $scope.gameOver = false;
      $scope.lastWinner = undefined;
    };

    $scope.startGame = function(playersForm) {
      if (playersForm.$valid) {
        $scope.playerOne = PlayersFct.get({name: $scope.playerOneName});
        $scope.playerTwo = PlayersFct.get({name: $scope.playerTwoName});
        $q.all([$scope.playerOne.$promise, $scope.playerTwo.$promise]).then(function(responses) {
          if ($scope.playerOne._id && $scope.playerTwo._id) {
            initializeGame();
            playersForm.$setPristine();
            playersForm.$setUntouched();
          } else {
            var promises = [];
            if (!$scope.playerOne._id) {
              PlayersFct.save({name: $scope.playerOneName}).$promise.then(function() {
                $scope.playerOne = PlayersFct.get({name: $scope.playerOneName});
                promises.push($scope.playerOne.$promise);
                initializeGame(promises);
                playersForm.$setPristine();
                playersForm.$setUntouched();
              });
            }
            if (!$scope.playerTwo._id) {
              PlayersFct.save({name: $scope.playerTwoName}).$promise.then(function() {
                $scope.playerTwo = PlayersFct.get({name: $scope.playerTwoName});
                promises.push($scope.playerTwo.$promise);
                initializeGame(promises);
                playersForm.$setPristine();
                playersForm.$setUntouched();
              });
            }
          }
        });
      }
    };

    function initializeGame(promises) {
      if (promises) {
        $q.all(promises).then(function(){
          $scope.players = PlayersFct.query();
          $scope.game.started = new Date();
          $scope.game.playerOne = $scope.playerOne._id;
          $scope.game.playerTwo = $scope.playerTwo._id;
          $scope.newRound();
        });
      } else {
        $scope.game.started = new Date();
        $scope.game.playerOne = $scope.playerOne._id;
        $scope.game.playerTwo = $scope.playerTwo._id;
        $scope.newRound();
      }
    }

    $scope.newRound = function() {
      $scope.currentRound = RoundSrv.getNewRound();
    };

    $scope.finishRound = function() {
      //Codigo
      console.log('Round finished!... starting next round');
      $scope.game.rounds.push($scope.currentRound);
      var winner = PlaySrv.getWinner($scope.game);
      if (winner) {
        $scope.game.winner = winner;
        $scope.game.finalized = new Date();
        GamesFct.save($scope.game).$promise.then(function() {
          //Final del juego
          $scope.gameOver = true;
          $scope.lastWinner = PlaySrv.getPlayerById($scope.game.winner, $scope.players);
        });
      } else {
        $scope.newRound();
      }
    };

    $scope.playAgain = function() {
      $scope.newGame();
    };

    $scope.getPlayer = function(playerId) {
      return PlaySrv.getPlayerById(playerId, $scope.players);
    };

    init();
  }]);
}());

