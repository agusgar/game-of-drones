(function() {
  'use strict';

  var appRoutes = angular.module('app.routes', []);

  appRoutes.config(
    function ($routeProvider) {
      $routeProvider
        .when('/',
        {
          controller: 'IndexCtrl',
          templateUrl: '/resource/index/index.tpl.html'
        })
        .when('/play',
        {
          controller: 'PlayCtrl',
          templateUrl: '/resource/play/play.tpl.html'
        })
        .when('/options',
        {
          controller: 'OptionsCtrl',
          templateUrl: '/resource/options/options.tpl.html'
        })
        .when('/scores',
        {
          controller: 'ScoresCtrl',
          templateUrl: '/resource/scores/scores.tpl.html'
        })
        .otherwise({ redirectTo: '/' });
    });
}());
(function() {
  'use strict';

  angular.module('app.core', []);

  angular.module('GameOfDrones', [
    'ngRoute',
    'ngResource',
    'ngAnimate',
    'ngAria',
    'ngMessages',
    'app.routes',
    'app.core',
    'ui.bootstrap'
  ]);
}());
