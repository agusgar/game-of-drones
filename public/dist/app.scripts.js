(function() {
  'use strict';

  angular.module('app.core').controller('AppCtrl', ['$scope', function($scope) {

  }]);
}());

(function() {
  'use strict';

  angular.module('app.core').controller('AskCtrl', ['$scope', '$uibModalInstance', 'modalTitle', 'modalText', 'modalQuestion', function($scope, $uibModalInstance, modalTitle, modalText, modalQuestion) {
    $scope.modalTitle = modalTitle;
    $scope.modalText = modalText;
    $scope.modalQuestion = modalQuestion;

    $scope.accept = function () {
      $uibModalInstance.close(true);
    };
    $scope.cancel = function() {
      $uibModalInstance.close(false);
    };
  }]);
}());



angular.module('app.core').directive('godAcceptAction', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionTitle', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionText', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionQuestion', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godModalTrigger', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godDecisionMake', ['$uibModal', function($uibModal) {
  return {
    restrict: 'A',
    require: [
      '^godDecisionTitle',
      '^godDecisionText',
      '^godDecisionQuestion',
      '^godAcceptAction',
      '^godModalTrigger'
    ],
    scope: {
      godDecisionTitle: '=',
      godDecisionText: '=',
      godDecisionQuestion: '=',
      godAcceptAction: '&',
      godModalTrigger: '='
    },
    controller: ['$scope', function($scope) {}],
    link: function(scope, iElement, iAttrs, ctrl) {
      scope.values = {
        decisionTitle: scope.godDecisionTitle,
        decisionText: scope.godDecisionText,
        decisionQuestion: scope.godDecisionQuestion
      };

      scope.$watch('godModalTrigger', function(newVal, oldVal) {
        if (newVal != oldVal) {
          var modalDecision = $uibModal.open({
            templateUrl: '/resource/ask/ask.tpl.html',
            controller: 'AskCtrl',
            size: 'md',
            scope: scope,
            resolve: {
              modalTitle: function() {
                return scope.values.decisionTitle;
              },
              modalText: function() {
                return scope.values.decisionText;
              },
              modalQuestion: function() {
                return scope.values.decisionQuestion;
              }
            }
          });

          modalDecision.result.then(function (accept) {
            if (accept) {
              scope.godAcceptAction();
            }
          });
        }
      });
    }
  };
}]);
(function() {
  'use strict';

  angular.module('app.core').factory('GamesFct', ['$resource', function($resource) {
    return $resource('/rest/games/:id', null, {
      'update': { method:'PUT' }
    });
  }]);
}());


(function() {
  'use strict';

  angular.module('app.core').factory('MovesFct', ['$resource', function($resource) {
    return $resource('/rest/moves/:id', null, {
      'update': { method:'PUT' }
    });
  }]);
}());


(function() {
  'use strict';

  angular.module('app.core').factory('PlayersFct', ['$resource', function($resource) {
    return $resource('/rest/players/:id', null, {
      'update': { method:'PUT' }
    });
  }]);
}());


(function() {
  'use strict';

  angular.module('app.core').controller('IndexCtrl', ['$scope', function($scope) {

  }]);
}());
(function(){
  'use strict';

  angular.module('app.core').controller('NotificationsCtrl', ['$scope', '$rootScope', '$timeout', 'NotificationsSrv', function($scope, $rootScope, $timeout, NotificationsSrv) {
    $scope.notifications = NotificationsSrv.getNotifications();

    $rootScope.showSuccess = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'success');
    };

    $rootScope.showError = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'error');
    };

    $rootScope.showInfo = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'info');
    };

    $rootScope.showWarning = function(text, time) {
      $scope.notifications.clearPromise = setAndClearNotifications(text, time, 'warning');
    };

    $rootScope.clearNotifications = function() {
      clearNotifications();
    };

    function setAndClearNotifications(text, time, type) {
      clearNotifications();
      if (time !== 0 && !time) {
        time = 15000;
      }
      $scope.notifications[type].text = text;
      $scope.notifications[type].active = true;
      $scope.notifications.clearPromise = $timeout(function() {
        $scope.notifications[type].text = "";
        $scope.notifications[type].active = false;
      }, time);
    }

    function clearNotifications() {
      $timeout.cancel($scope.notifications.clearPromise);

      $scope.notifications.success.text = "";
      $scope.notifications.error.text = "";
      $scope.notifications.info.text = "";
      $scope.notifications.warning.text = "";

      $scope.notifications.success.active = false;
      $scope.notifications.error.active = false;
      $scope.notifications.info.active = false;
      $scope.notifications.warning.active = false;
    }


  }]);
}());
(function() {
  'use strict';

  angular.module('app.core').service('NotificationsSrv', function() {

    this.getNotifications = function() {
      return {
        success: {
          active: false,
          text: ''
        },
        error: {
          active: false,
          text: ''
        },
        info: {
          active: false,
          text: ''
        },
        warning: {
          active: false,
          text: ''
        },
        clearPromise: undefined
      };
    };

  });
}());

(function() {
  'use strict';

  angular.module('app.core').controller('MoveEditionCtrl', ['$scope', '$uibModalInstance', 'move', 'moves', 'title', function($scope, $uibModalInstance, move, moves, title) {
    $scope.move = move;
    $scope.title = title;
    $scope.moves = moves;

    $scope.save = function() {
      $uibModalInstance.close($scope.move);
    };

    $scope.cancel = function() {
      $uibModalInstance.dismiss();
    };
  }]);
}());
(function() {
  'use strict';

  angular.module('app.core').controller('OptionsCtrl', ['$scope', 'MovesFct', '$uibModal', 'OptionsSrv', function($scope, MovesFct, $uibModal, OptionsSrv) {
    function init() {
      $scope.moves = MovesFct.query();
    }

    $scope.openCreateMove = function(move) {
      var newMove = {name: '', kill: $scope.moves[0]._id};
      var modalMoveEdition = $uibModal.open({
        templateUrl: '/resource/options/moveEdition.tpl.html',
        controller: 'MoveEditionCtrl',
        size: 'lg',
        resolve: {
          move: function() {
            return newMove;
          },
          moves: function() {
            return $scope.moves;
          },
          title: function() {
            return 'New Move';
          }
        }
      });

      modalMoveEdition.result.then(function(newMove) {
        MovesFct.save(newMove).$promise.then(function(response) {
          $scope.moves = MovesFct.query();
        });
      });
    };

    $scope.openUpdateMove = function(move) {
      var auxMove = angular.copy(move);

      var modalMoveEdition = $uibModal.open({
        templateUrl: '/resource/options/moveEdition.tpl.html',
        controller: 'MoveEditionCtrl',
        size: 'lg',
        resolve: {
          move: function() {
            return auxMove;
          },
          moves: function() {
            return $scope.moves;
          },
          title: function() {
            return 'Edit Move';
          }
        }
      });

      modalMoveEdition.result.then(function(updatedMove) {
        MovesFct.update({id: move._id}, updatedMove).$promise.then(function(response) {
          angular.copy(updatedMove, move);
        });
      });
    };

    $scope.getMoveById = function(moveId) {
      return OptionsSrv.getMoveById(moveId, $scope.moves);
    };

    init();
  }]);
}());
(function() {
  'use strict';

  angular.module('app.core').service('OptionsSrv', [function() {
    this.getMoveById = function(moveId, moves) {
      var move;
      for(var i = 0; i < moves.length && !move; i++) {
        if (moves[i]._id == moveId) {
          move = moves[i];
        }
      }
      return move;
    };

  }]);
}());
(function() {
  'use strict';

  angular.module('app.core').controller('PlayCtrl', ['$scope', '$q', 'PlaySrv', 'RoundSrv', 'GamesFct', 'PlayersFct', function($scope, $q, PlaySrv, RoundSrv, GamesFct, PlayersFct) {
    function init() {
      $scope.newGame();
    }

    $scope.newGame = function() {
      $scope.game = PlaySrv.getNewGame();
      $scope.players = PlayersFct.query();
      $scope.playerOneName= "";
      $scope.playerTwoName = "";
      $scope.playerOne = undefined;
      $scope.playerTwo = undefined;
      $scope.gameOver = false;
      $scope.lastWinner = undefined;
    };

    $scope.startGame = function(playersForm) {
      if (playersForm.$valid) {
        $scope.playerOne = PlayersFct.get({name: $scope.playerOneName});
        $scope.playerTwo = PlayersFct.get({name: $scope.playerTwoName});
        $q.all([$scope.playerOne.$promise, $scope.playerTwo.$promise]).then(function(responses) {
          if ($scope.playerOne._id && $scope.playerTwo._id) {
            initializeGame();
            playersForm.$setPristine();
            playersForm.$setUntouched();
          } else {
            var promises = [];
            if (!$scope.playerOne._id) {
              PlayersFct.save({name: $scope.playerOneName}).$promise.then(function() {
                $scope.playerOne = PlayersFct.get({name: $scope.playerOneName});
                promises.push($scope.playerOne.$promise);
                initializeGame(promises);
                playersForm.$setPristine();
                playersForm.$setUntouched();
              });
            }
            if (!$scope.playerTwo._id) {
              PlayersFct.save({name: $scope.playerTwoName}).$promise.then(function() {
                $scope.playerTwo = PlayersFct.get({name: $scope.playerTwoName});
                promises.push($scope.playerTwo.$promise);
                initializeGame(promises);
                playersForm.$setPristine();
                playersForm.$setUntouched();
              });
            }
          }
        });
      }
    };

    function initializeGame(promises) {
      if (promises) {
        $q.all(promises).then(function(){
          $scope.players = PlayersFct.query();
          $scope.game.started = new Date();
          $scope.game.playerOne = $scope.playerOne._id;
          $scope.game.playerTwo = $scope.playerTwo._id;
          $scope.newRound();
        });
      } else {
        $scope.game.started = new Date();
        $scope.game.playerOne = $scope.playerOne._id;
        $scope.game.playerTwo = $scope.playerTwo._id;
        $scope.newRound();
      }
    }

    $scope.newRound = function() {
      $scope.currentRound = RoundSrv.getNewRound();
    };

    $scope.finishRound = function() {
      //Codigo
      console.log('Round finished!... starting next round');
      $scope.game.rounds.push($scope.currentRound);
      var winner = PlaySrv.getWinner($scope.game);
      if (winner) {
        $scope.game.winner = winner;
        $scope.game.finalized = new Date();
        GamesFct.save($scope.game).$promise.then(function() {
          //Final del juego
          $scope.gameOver = true;
          $scope.lastWinner = PlaySrv.getPlayerById($scope.game.winner, $scope.players);
        });
      } else {
        $scope.newRound();
      }
    };

    $scope.playAgain = function() {
      $scope.newGame();
    };

    $scope.getPlayer = function(playerId) {
      return PlaySrv.getPlayerById(playerId, $scope.players);
    };

    init();
  }]);
}());


(function() {
  'use strict';

  angular.module('app.core').service('PlaySrv', [function() {
    this.getNewGame = function() {
      return {
        "playerOne" : undefined,
        "playerTwo" : undefined,
        "started" : undefined,
        "finalized" : undefined,
        "winner": undefined,
        "rounds" : []
      };
    };

    this.getWinner = function(game) {
      var players = {};
      players[game.playerOne] = 0;
      players[game.playerTwo] = 0;
      var winner;
      for (var i = 0; i < game.rounds.length && !winner; i++) {
        var round = game.rounds[i];
        if (round.winner) {
          players[round.winner] += 1;
          if (players[round.winner] >= 3) {
            winner = round.winner;
          }
        }
      }
      return winner;
    };

    this.getPlayerById = function(playerId, players) {
      var player;
      for(var i = 0; i < players.length && !player; i++) {
        if (players[i]._id == playerId) {
          player = players[i];
        }
      }
      return player;
    };

  }]);
}());
//(function() {
//  'use strict';
//
//  angular.module('app.core').controller('RoundCtrl', ['$scope', 'RoundSrv', 'MovesFct', function($scope, RoundSrv, MovesFct) {
//    $scope.moves = MovesFct.query();
//
//    $scope.finishMove = function(move) {
//      if (!$scope.currentRound.movePlayerOne) {
//        $scope.currentRound.movePlayerOne = move;
//      } else {
//        $scope.currentRound.movePlayerTwo = move;
//        $scope.currentRound.winner = RoundSrv.getWinner();
//        $scope.finishRound($scope.currentRound);
//      }
//    };
//  }]);
//}());

angular.module('app.core').directive('godGame', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godCurrentRound', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godPlayerOne', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godPlayerTwo', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godFinishRound', function() {
  return {
    controller: ['$scope', function($scope) {}]
  };
});

angular.module('app.core').directive('godRound', ['RoundSrv', 'MovesFct', function(RoundSrv, MovesFct) {
  return {
    restrict: 'A',
    templateUrl: '/resource/round/round.tpl.html',
    require: [
      '^godGame',
      '^godCurrentRound',
      '^godPlayerOne',
      '^godPlayerTwo',
      '^godFinishRound'
    ],
    scope: {
      godGame: '=',
      godCurrentRound: '=',
      godPlayerOne: '=',
      godPlayerTwo: '=',
      godFinishRound: '&'
    },
    controller: ['$scope', function($scope) {}],
    link: function(scope, iElement, iAttrs, ctrl) {
      scope.moves = MovesFct.query();
      scope.moves.$promise.then(function() {
        scope.move = scope.moves[0];
      });

      scope.finishMove = function(move) {
        if (!scope.godCurrentRound.movePlayerOne) {
          scope.godCurrentRound.movePlayerOne = move;
        } else {
          scope.godCurrentRound.movePlayerTwo = move;
          scope.godCurrentRound.winner = RoundSrv.getWinner(scope.godCurrentRound, scope.godPlayerOne, scope.godPlayerTwo);
          if (scope.godCurrentRound.winner) {
            console.log('Current round winner: ' + scope.godCurrentRound.winner);
            scope.godFinishRound();
          } else {
            scope.godCurrentRound.movePlayerOne = undefined;
            scope.godCurrentRound.movePlayerTwo = undefined;
          }
        }
        scope.move = scope.moves[0];
      };
    }
  };
}]);
(function() {
  'use strict';

  angular.module('app.core').service('RoundSrv', [function() {
    this.getNewRound = function() {
      return {
        "movePlayerOne" : undefined,
        "movePlayerTwo" : undefined,
        "winner" : undefined
      };
    };

    this.getWinner = function(round, playerOne, playerTwo) {
      if (round.movePlayerOne.kill == round.movePlayerTwo._id) {
        return playerOne._id;
      } else if (round.movePlayerTwo.kill == round.movePlayerOne._id) {
        return playerTwo._id;
      } else {
        return undefined;
      }
    };
  }]);
}());
(function() {
  'use strict';

  angular.module('app.core').controller('ScoresCtrl', ['$scope', '$q', 'PlayersFct', 'GamesFct', 'ScoresSrv', function($scope, $q, PlayersFct, GamesFct, ScoresSrv) {
    $scope.players = PlayersFct.query();
    $scope.games = GamesFct.query();

    $q.all([$scope.players.$promise, $scope.games.$promise]).then(function() {
      $scope.scores = ScoresSrv.getPlayerScores($scope.games, $scope.players);
    });

  }]);
}());
(function() {
  'use strict';

  angular.module('app.core').service('ScoresSrv', [function() {
    this.getPlayerScores = function(games, players) {
      var scores = {};
      for(var i = 0; i < players.length; i++) {
        if (!scores[players[i]._id]) {
          scores[players[i]._id] = {
            victories: 0,
            played: 0
          };
        }
      }

      for(var j = 0; j < games.length; j++) {
        var game = games[j];
        var playerOne = game.playerOne;
        var playerTwo = game.playerTwo;
        var winner = game.winner;

        scores[playerOne].played += 1;
        scores[playerTwo].played += 1;

        if (playerOne == winner) {
          scores[playerOne].victories += 1;
        } else if (playerTwo == winner) {
          scores[playerTwo].victories += 1;
        }
      }

      return scores;
    };
  }]);
}());