var express = require('express');
var bodyParser = require('body-parser');
var config = require(__dirname + '/app/config/env.properties');
var envProperties = config.envProperties();

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('title', 'GameOfDrones');
app.set('views', __dirname + '/public/app/modules');
app.use('/config', express.static(__dirname + '/public/app/config'));
app.use('/libs', express.static(__dirname + '/public/libs'));
app.use('/resource', express.static(__dirname + '/public/app/modules'));
app.use('/dist', express.static(__dirname + '/public/dist'));

var router = express.Router();

router.use('/rest', require('./app/controllers/index'));
app.use(router);

//app.register('.html', require('jade'));

app.get('/', function (req, res) {
  res.render('app/app.tpl.jade', {env: "PROD"});
});

var server = app.listen(envProperties.port, function(){
  console.log('Listening on port %d', server.address().port);
});

