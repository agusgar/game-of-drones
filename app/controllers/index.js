var express = require('express');

var router = express.Router();

router.use('/players', require('./players'));
router.use('/moves', require('./moves'));
//router.use('/rounds', require('./rounds'));
router.use('/games', require('./games'));

module.exports = router;