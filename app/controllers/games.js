var express = require('express');
var router = express.Router();
var game = require('../models/game');

router

  .post('/', function (req, res) {
    game.newGame(req.body, function(err) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    });
  })

  .get('/', function(req, res) {
    game.getAll(function(err, games) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(games);
      }

    });
  })

  .get('/:game_id', function(req, res) {
    game.findById(req.params.game_id, function(err, json) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(json);
      }
    });
  })

  .put('/:game_id', function(req, res) {
    game.updateGame(req.params.game_id, req.body, function(err, json) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(json)
      }
    });
  })

  .delete('/:game_id', function(req, res) {
    game.deleteGame(req.params.game_id, function(err) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.send(200);
      }
    });
  });

module.exports = router;