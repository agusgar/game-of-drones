var express = require('express');
var router = express.Router();
var player = require('../models/player');

router

  .post('/', function (req, res) {
    player.newPlayer(req.body, function(err) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    });
  })

  .get('/', function(req, res) {
    if (req.query && req.query.name) {
      player.findByName(req.query.name, function(err, player) {
        if (err) {
          res.sendStatus(500);
        } else {
          res.json(player);
        }
      });
    } else {
      player.getAll(function(err, players) {
        if (err) {
          res.sendStatus(500);
        } else {
          res.json(players);
        }

      });
    }

  })

  .get('/:player_id', function(req, res) {
    player.findById(req.params.player_id, function(err, json) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(json);
      }
    });
  })

  .put('/:player_id', function(req, res) {
    player.updatePlayer(req.params.player_id, req.body, function(err, json) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(json)
      }
    });
  })

  .delete('/:player_id', function(req, res) {
    player.deletePlayer(req.params.player_id, function(err) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.send(200);
      }
    });
  });

module.exports = router;