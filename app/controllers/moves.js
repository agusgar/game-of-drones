var express = require('express');
var router = express.Router();
var move = require('../models/move');

router

  .post('/', function (req, res) {
    move.newMove(req.body, function(err) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    });
  })

  .get('/', function(req, res) {
    move.getAll(function(err, moves) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(moves);
      }

    });
  })

  .get('/:move_id', function(req, res) {
    move.findById(req.params.move_id, function(err, json) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(json);
      }
    });
  })

  .put('/:move_id', function(req, res) {
    move.updateMove(req.params.move_id, req.body, function(err, json) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.json(json)
      }
    });
  })

  .delete('/:move_id', function(req, res) {
    move.deleteMove(req.params.move_id, function(err) {
      if (err) {
        res.sendStatus(500);
      } else {
        res.send(200);
      }
    });
  });

module.exports = router;