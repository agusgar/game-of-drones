var env = require('./env.properties.json');

exports.envProperties = function() {
  var currEnv = "LOCAL";
  return env[currEnv];
};