var mongoose = require('mongoose');
var Grid = require('gridfs-stream');

mongoose.connect('mongodb://localhost/gameofdrones');
var db = mongoose.connection;
var Schema = mongoose.Schema;

Grid.mongo = mongoose.mongo;
var gfs = Grid(db.db);

db.on('error', function(err) {
  console.log(err.message);
});

db.on('open', function() {
  console.log("mongodb connection opened correctly");
});

exports.getDb = function() {
  return db;
};

exports.getSchema = function() {
  return Schema;
};

exports.getGFS = function() {
  return gfs;
};
