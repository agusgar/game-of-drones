var mongoose = require('mongoose');
var db = require('../config/db');
var Schema = db.getSchema();

var roundSchema = new Schema({
  movePlayerOne: { type: Schema.Types.Object, ref: 'Move' },
  movePlayerTwo: { type: Schema.Types.Object, ref: 'Move' },
  winner: { type: Schema.Types.ObjectId, ref: 'Player' }
});

var gameSchema = new Schema({
  playerOne: { type: Schema.Types.ObjectId, ref: 'Player' },
  playerTwo: { type: Schema.Types.ObjectId, ref: 'Player' },
  winner: { type: Schema.Types.ObjectId, ref: 'Player' },
  rounds: [roundSchema],
  started: { type: Date },
  finalized: { type: Date }
});

var Game = mongoose.model('Game', gameSchema);


exports.getGameSchema = function() {
  return gameSchema;
};

exports.newGame = function(game, callback) {
  var someGame = new Game(game);
  someGame.save(callback);
};

exports.getAll = function(callback) {
  Game.find([], callback);
};

exports.findById = function(gameId, callback) {
  Game.findById(gameId).exec(callback);
};

exports.findByName = function(gameName, callback) {
  Game.findOne({name: gameName}).exec(callback);
};

exports.updateGame = function(gameId, game , callback) {
  Game.findByIdAndUpdate(gameId, game).exec(callback);
};

exports.deleteGame = function(gameId, callback) {
  Game.findByIdAndRemove(gameId).exec(callback);
};

