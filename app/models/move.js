var mongoose = require('mongoose');
var db = require('../config/db');
var Schema = db.getSchema();

var moveSchema = new Schema({
  name: { type: String, required: true, index: { unique: true } },
  kill: { type: Schema.Types.ObjectId, ref: 'Move' },
  updated: { type: Date, default: Date.now }
});

var Move = mongoose.model('Move', moveSchema);

Move.find([], function(err, response) {
  if (response.length == 0) {
    var rock = new Move({_id: '231231231231', name: 'Paper', kill: '321321321321'});
    var paper = new Move({_id: '123123123123', name: 'Scissors', kill: '231231231231'});
    var scissors = new Move({_id: '321321321321', name: 'Rock', kill: '123123123123'});

    rock.save();
    paper.save();
    scissors.save();
  }
});

exports.getMoveSchema = function() {
  return moveSchema;
};

exports.newMove = function(move, callback) {
  var someMove = new Move(move);
  someMove.save(callback);
};

exports.getAll = function(callback) {
  Move.find([], callback);
};

exports.findById = function(moveId, callback) {
  Move.findById(moveId).exec(callback);
};

exports.findByName = function(moveName, callback) {
  Move.findOne({name: moveName}).exec(callback);
};

exports.updateMove = function(moveId, move , callback) {
  Move.findByIdAndUpdate(moveId, move).exec(callback);
};

exports.deleteMove = function(moveId, callback) {
  Move.findByIdAndRemove(moveId).exec(callback);
};

