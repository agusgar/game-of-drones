var mongoose = require('mongoose');
var db = require('../config/db');
var Schema = db.getSchema();

var playerSchema = new Schema({
  name: { type: String, required: true, index: { unique: true } },
  updated: { type: Date, default: Date.now }
});

var Player = mongoose.model('Player', playerSchema);

exports.getPlayerSchema = function() {
  return playerSchema;
};

exports.newPlayer = function(player, callback) {
  var somePlayer = new Player(player);
  somePlayer.save(callback);
};

exports.getAll = function(callback) {
  Player.find([], callback);
};

exports.findById = function(playerId, callback) {
  Player.findById(playerId).exec(callback);
};

exports.findByName = function(playerName, callback) {
  Player.findOne({name: playerName}).exec(callback);
};

exports.updatePlayer = function(playerId, player , callback) {
  Player.findByIdAndUpdate(playerId, player).exec(callback);
};

exports.deletePlayer = function(playerId, callback) {
  Player.findByIdAndRemove(playerId).exec(callback);
};

